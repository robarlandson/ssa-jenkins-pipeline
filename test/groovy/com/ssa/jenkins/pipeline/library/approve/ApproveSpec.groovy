package com.ssa.jenkins.pipeline.library.approve

import spock.lang.Specification

class ApproveSpec extends Specification {

	
	def "jenkins context is available"() {
		given: "Default jenkins context"
		def jenkins = [ echo : 'hello' ]
		when: 'Creating class with jenkins context'
		def approve = new Approve(jenkins)
		then: "Jenkins context is avaliable"
		approve.getJenkins() == jenkins
	}
	
	def "addApproval is available"() {
		given: "Approval addApproval method"
		def jenkins = [ 
			echo: {},
			timeout: { time, unit ->  },
			env : [
				BUILD_NUMBER: 2	
			]
		]
		def approve = new Approve(jenkins)
		when: 'Its called with build number of 2'
		approve.addApproval([:])
		then: "The config should have BUILD_NUMBER = 2"
		approve.getConfig()['description'] == "Approve build ${jenkins.env.BUILD_NUMBER}"
		
		
	}
	
}


