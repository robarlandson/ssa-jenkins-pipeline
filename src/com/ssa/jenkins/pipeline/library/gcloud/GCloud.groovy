package com.ssa.jenkins.pipeline.library.gcloud

class GCloud implements Serializable {

    def jenkins

    GCloud() { }

    GCloud(jenkins) {
        this.jenkins = jenkins
    }

    def getConfig(Map<String, Object> params) {
        def defaults = [
            user: 'robarlandson',
            server: 'test',
            project: 'eresidentcare-01',
            keyFile: '$FILE',
            gCloudHome: '$GCLOUD_HOME',
            zone: 'us-central1-c',
            artifact: 'dist/ROOT.war',
            artifactLocation: 'deploymentWars',
            backUpLocation: 'backupWars',
            tomcatLocation: '/usr/lib/tomcat7er'
        ]
        def config = defaults + params
        return config
    }

    def authenticate(Map<String, Object> params) {
        def config = this.getConfig(params)       
        jenkins.sh("${config.gCloudHome} auth activate-service-account --key-file=${config.keyFile}")
        jenkins.sh("${config.gCloudHome} config set project ${config.project}")
    }

    def getVersion(Map<String, Object> params) {
        def config = this.getConfig(params)
        jenkins.sh("sh ${config.gCloudHome} --version")
    }

    def stopServer(Map<String, Object> params) {
        def config = this.getConfig(params)
        jenkins.sh("${config.gCloudHome} compute ssh ${config.user}@${config.server} --zone=${config.zone} -- sudo ${config.tomcatLocation}/bin/shutdown.sh")
    }

    def startServer(Map<String, Object> params) {
        def config = this.getConfig(params)
        jenkins.sh("${config.gCloudHome} compute ssh ${config.user}@${config.server} --zone=${config.zone} -- sudo ${config.tomcatLocation}/bin/startup.sh")
    }

    def copyArtifacts(Map<String, Object> params) {
        def config = this.getConfig(params)
        jenkins.sh("${config.gCloudHome} compute scp ${config.artifact} ${config.user}@${config.server}:${config.artifactLocation} --zone=${config.zone}")
    }

    def backup(Map<String, Object> params) {
        def config = this.getConfig(params)
        jenkins.sh("${config.gCloudHome} compute ssh ${config.user}@${config.server} --zone=${config.zone} -- sudo cp ${config.tomcatLocation}/webapps/ROOT.war ${config.backUpLocation}/ROOT.war.build-${jenkins.env.BUILD_NUMBER}")
    }

    def clearCurrent(Map<String, Object> params) {
        def config = this.getConfig(params)
        this.backup(params)
        jenkins.sh("${config.gCloudHome} compute ssh ${config.user}@${config.server} --zone=${config.zone} -- sudo rm -r ${config.tomcatLocation}/webapps/ROOT")
        jenkins.sh("${config.gCloudHome} compute ssh ${config.user}@${config.server} --zone=${config.zone} -- sudo rm -r ${config.tomcatLocation}/webapps/ROOT.war")
        jenkins.sh("${config.gCloudHome} compute ssh ${config.user}@${config.server} --zone=${config.zone} -- sudo rm -r ${config.tomcatLocation}/work/Catalina/localhost")
        jenkins.sh("${config.gCloudHome} compute ssh ${config.user}@${config.server} --zone=${config.zone} -- sudo rm -r ${config.tomcatLocation}/temp/*")
    }    

    def install(Map<String, Object> params) {
        def config = this.getConfig(params)
        jenkins.sh("${config.gCloudHome} compute ssh ${config.user}@${config.server} --zone=${config.zone} -- sudo cp ${config.artifactLocation}/ROOT.war ${config.tomcatLocation}/webapps")
        this.startServer(params)
        jenkins.sh("${config.gCloudHome} compute ssh ${config.user}@${config.server} --zone=${config.zone} -- sleep 10")
        jenkins.sh("${config.gCloudHome} compute ssh ${config.user}@${config.server} --zone=${config.zone} -- sudo ln -s /usr/src/user-files/eFile ${config.tomcatLocation}/webapps/ROOT/eFile")
        jenkins.sh("${config.gCloudHome} compute ssh ${config.user}@${config.server} --zone=${config.zone} -- sudo ln -s /usr/src/user-files/images ${config.tomcatLocation}/webapps/ROOT/images")
        jenkins.sh("${config.gCloudHome} compute ssh ${config.user}@${config.server} --zone=${config.zone} -- sudo ln -s /usr/src/user-files/eLog ${config.tomcatLocation}/webapps/ROOT/eLog")
        jenkins.sh("${config.gCloudHome} compute ssh ${config.user}@${config.server} --zone=${config.zone} -- sudo ln -s /usr/src/user-files/upload ${config.tomcatLocation}/webapps/ROOT/upload")
        jenkins.sh("${config.gCloudHome} compute ssh ${config.user}@${config.server} --zone=${config.zone} -- sudo ln -s /usr/src/user-files/HeaderImage ${config.tomcatLocation}/webapps/ROOT/HeaderImage")
        this.stopServer(params)
        this.startServer(params)
    }

}