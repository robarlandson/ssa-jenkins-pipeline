package com.ssa.jenkins.pipeline.library.gsutil

class GSutil implements Serializable {

    def jenkins

    GSutil() { }

    GSutil(jenkins) {
        this.jenkins = jenkins
    }

    def getConfig(Map<String, Object> params) {
        def defaults = [
            project: 'eresidentcare-01',
            keyFile: '$FILE',
            gsUtilHome: '$GSUTIL_HOME',
            bucketName: 'app-test.eresidentcare.com',
            directory: './build/*'
        ]
        def config = defaults + params
        return config
    }

    def copyDirectoryToBucket(Map<String, Object> params) {
        def config = this.getConfig(params)
        jenkins.sh("${config.gsUtilHome} cp -R ${config.directory} gs://${config.bucketName}")
        jenkins.sh("${config.gsUtilHome} web set -m index.html -e index.html gs://${config.bucketName}")
    }

    def setDefaultIndexAnd404(Map<String, Object> params) {
        def config = this.getConfig(params)
        jenkins.sh("${config.gsUtilHome} web set -m index.html -e index.html gs://${config.bucketName}")
    }

    def clearBucket(Map<String, Object> params) {
        def config = this.getConfig(params)
        jenkins.sh("${config.gsUtilHome} rm -R gs://${config.bucketName}/*")
    }
}