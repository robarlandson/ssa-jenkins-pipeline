package com.ssa.jenkins.pipeline.library.approve


class Approve implements Serializable {

	def jenkins
	def config
	
	Approve() throws Exception {
		throw new Exception("'this' must be passed when creating a new instance")
	}
	
	Approve(jenkins) {
		this.jenkins = jenkins
	}
	
	def addApproval(Map<String, Object> params) {
		
		def defaults = [
				time			: 1,
				unit			: 'DAYS',
				message			: 'Approve?',
				defaultValue	: "Comment on ${jenkins.env.BUILD_NUMBER} Approval",
				description		: "Approve build ${jenkins.env.BUILD_NUMBER}",	
				submitter		: ''
			]
			
		config = defaults + params
			
		jenkins.echo "Approval arguments: $config"
		
		jenkins.timeout(time: config.time, unit: config.unit) {
			def userInput = jenkins.input([
				message			: config.message,
				parameters		: [
					jenkins.string([
						defaultValue : config.defaultValue,
						descriptions : config.description,
						name		 : 'APPROVAL_COMMENTS'	
					])					
				],
				submitter			 : config.submitter,
				submitterParameter   : 'APPROVAL'					
			])
			submitterParameter: 'APPROVER'
			jenkins.echo "Approved by ${userInput['APPROVER']}, with comments: ${userInput['APPROVAL_COMMENTS']}"
		}
	}	
}



