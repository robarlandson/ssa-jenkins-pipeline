import com.ssa.jenkins.pipeline.library.gcloud.GCloud

def call(Map<String, Object> params) {
	GCloud gcloud = new GCloud(this)
	gcloud.copyArtifacts(params)
    gcloud.stopServer(params)
    gcloud.clearCurrent(params)
    gcloud.install(params)
}
