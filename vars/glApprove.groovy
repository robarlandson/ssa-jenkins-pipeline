import com.ssa.jenkins.pipeline.library.approve.Approve


def call(Map<String, Object> params) {
	Approve approve = new Approve(this)
	approve.addApproval(params)
}