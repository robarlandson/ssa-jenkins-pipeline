import com.ssa.jenkins.pipeline.library.gsutil.GSutil

def call(Map<String, Object> params) {
	GSutil gsutil = new GSutil(this)
	gsutil.copyDirectoryToBucket(params)
}